# [Tilix](https://gnunn1.github.io/tilix-web/)
if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
    source /etc/profile.d/vte.sh
fi
# [ble.sh](https://github.com/akinomyoga/ble.sh)
if [ -d ~/.ble ]; then
    source ~/.ble/ble.sh
fi

# [Deno](https://deno.land)
if [ -d $HOME/.deno/bin ]; then
    export PATH="$HOME/.deno/bin:$PATH"
fi
# [Yarn](https://yarnpkg.com/)
if [ -d $HOME/.yarn/bin ]; then
    export PATH="$HOME/.yarn/bin:$PATH"
fi
# [Python](https://www.python.org/)
if [ -d $HOME/.local/bin ]; then
    export PATH="$HOME/.local/bin:$PATH"
fi
# [Bun](https://github.com/oven-sh/bun)
if [ -d $HOME/.bun/bin ]; then
    export PATH="$HOME/.bun/bin:$PATH"
fi
# [Flatpak](https://github.com/flatpak/flatpak)
if [ -d /var/lib/flatpak/exports/bin ]; then
    export PATH="/var/lib/flatpak/exports/bin:$PATH"
fi

# [thefuck](https://github.com/nvbn/thefuck)
if command -v "thefuck" &> /dev/null; then
    eval $(thefuck --alias)
fi
# [Homebrew](https://brew.sh)
if [ -d /home/linuxbrew ]; then
    eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
fi
# [zoxide](https://github.com/ajeetdsouza/zoxide)
if command -v "zoxide" &> /dev/null; then
    eval "$(zoxide init bash)"
fi

# [bat](https://github.com/sharkdp/bat)
if command -v "bat" &> /dev/null; then
    alias cat="BAT_PAGER='less -RFSc' bat --wrap=never"
fi
# [pay-respects](https://codeberg.org/iff/pay-respects)
if command -v "pay-respects" &> /dev/null; then
    alias fuck="$(pay-respects bash)"
fi

# [Qt](https://launchpad.net/ubuntu/+source/qt6ct)
if command -v "qt6ct" &> /dev/null; then
    export QT_QPA_PLATFORMTHEME="qt6ct"
fi