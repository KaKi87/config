# Forgejo auto-upgrade script

Automatically download and run the latest Forgejo release.

## Setup

- Install the following programs :
  - [`bun`](https://bun.sh/)
  - `find` ([`findutils`](https://www.gnu.org/software/findutils/))
  - [`wget`](https://www.gnu.org/software/wget/)
  - [`cron`](https://salsa.debian.org/debian/cron)

- In your Forgejo instance directory :
  - Create the following files :
    - a [symbolic link](https://en.wikipedia.org/wiki/Symbolic_link) of any name to a Forgejo executable (e.g. `ln -s ./forgejo-x.y.z-linux-arch ./forgejo`)
    - a `restart.sh` script (e.g. containing `systemctl`/`screen` commands)
  - Download the [`upgrade.js`](./upgrade.js) script
- Add the script to your crontab (e.g. `0 * * * * bash -c "cd <path> && bun ./upgrade.js"` to make it run every hour).

### My setup

As I'm managing services with `screen`, I made a custom [`restart.sh`](./restart.sh) script that :

- stops the currently running screen session (if any) ;
- waits for the Forgejo port to be closed ;
- starts a new screen session ;
- waits for the Forgejo port to be open.

You may use it by providing the following parameters in a `.env` file :

- `SCREEN_NAME` : the `screen` session name
- `SYMLINK_NAME` : the name of the symbolic link to the Forgejo executable
- `PORT` : the Forgejo HTTP server's listening port.