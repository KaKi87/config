#!/usr/bin/env bash
export $(cat .env | xargs)
screen -x $SCREEN_NAME -X stuff "^C" || true
while screen -ls | grep -q $SCREEN_NAME; do
    sleep 1
done
screen -dmS $SCREEN_NAME bash -c "./$SYMLINK_NAME"
while ss -nat | grep "LISTEN" | grep -q $PORT; do
    sleep 1
done