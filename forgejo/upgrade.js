import * as semver from 'jsr:@std/semver';

const exec = async command => new TextDecoder().decode(
    (await (new Deno.Command(
        'bash',
        {
            args: [
                '-c',
                command
            ]
        }
    )).output()).stdout
);

let {
    executableName,
    symlinkPath
} = JSON.parse(await exec(`find . -maxdepth 1 -ilname "./forgejo*" -printf '{"executableName":"%l","symlinkPath":"%p"}'`));
executableName = executableName.slice(2);

const
    getVersion = executableName => executableName.split('-')[1],
    executableVersion = getVersion(executableName),
    latestData = await (await fetch('https://codeberg.org/api/v1/repos/forgejo/forgejo/releases/latest')).json(),
    latestExecutables = latestData['assets'].map(item => ({
        name: item.name,
        version: getVersion(item.name),
        platform: item.name.split(`forgejo-${latestData['tag_name'].slice(1)}-`)[1],
        url: item['browser_download_url']
    }));

if(!latestExecutables.find(item => item.name === executableName && semver.greaterThan(item.version, executableVersion))){
    const {
        url,
        name
    } = latestExecutables.find(item => executableName.endsWith(item.platform));
    await exec(`wget ${url} && chmod +x ./${name} && rm ${symlinkPath} && ln -s ./${name} ${symlinkPath} && ./restart.sh`);
}