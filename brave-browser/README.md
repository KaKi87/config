# Extensions

| [Free Download Manager](https://chrome.google.com/webstore/detail/ahmpjcflkgiildlgicmcieglgoilbfdp) | [Config](./ahmpjcflkgiildlgicmcieglgoilbfdp) |
| :--- | --- |
| [Silk - Privacy Pass Client](https://chrome.google.com/webstore/detail/ajhmfdgkijocedmfjonnpjfojldioehi) | [Config](./ajhmfdgkijocedmfjonnpjfojldioehi) |
| [BetterTTV](https://chrome.google.com/webstore/detail/ajopnjidmegmdimjlfnijceegpefgped) | [Config](./ajopnjidmegmdimjlfnijceegpefgped) |
| [Hoppscotch Browser Extension](https://chrome.google.com/webstore/detail/amknoiejhlmhancpahfcfcfhllgkpbld) | [Config](./amknoiejhlmhancpahfcfcfhllgkpbld) |
| [Redirect Path](https://chrome.google.com/webstore/detail/aomidfkchockcldhbkggjokdkkebmdll) | [Config](./aomidfkchockcldhbkggjokdkkebmdll) |
| [TWP - Translate Web Pages](https://chrome.google.com/webstore/detail/bolggfoncklhniejomgplkjcllmnonbh) | [Config](./bolggfoncklhniejomgplkjcllmnonbh) |
| [Nimbus Screenshot & Screen Video Recorder](https://chrome.google.com/webstore/detail/bpconcjcammlapcogcnnelfmaeghhagj) | [Config](./bpconcjcammlapcogcnnelfmaeghhagj) |
| [Vencord Web](https://chrome.google.com/webstore/detail/cbghhgpcnddeihccjmnadmkaejncjndb) | [Config](./cbghhgpcnddeihccjmnadmkaejncjndb) |
| [uBlock Origin](https://chrome.google.com/webstore/detail/cjpalhdlnbpafiamejdnhcphjbkeiagm) | [Config](./cjpalhdlnbpafiamejdnhcphjbkeiagm) |
| [Stylus](https://chrome.google.com/webstore/detail/clngdbkpkpeebahjckkjfobafhncgmne) | [Config](./clngdbkpkpeebahjckkjfobafhncgmne) |
| [Old Reddit Redirect](https://chrome.google.com/webstore/detail/dneaehbmnbhcippjikoajpoabadpodje) | [Config](./dneaehbmnbhcippjikoajpoabadpodje) |
| [Session Buddy](https://chrome.google.com/webstore/detail/edacconmaakjimmfgnblocblbcdcpbko) | [Config](./edacconmaakjimmfgnblocblbcdcpbko) |
| [JSON Viewer](https://chrome.google.com/webstore/detail/efknglbfhoddmmfabeihlemgekhhnabb) | [Config](./efknglbfhoddmmfabeihlemgekhhnabb) |
| [Dark Reader](https://chrome.google.com/webstore/detail/eimadpbcbfnmbkopoojfekhnkhdbieeh) | [Config](./eimadpbcbfnmbkopoojfekhnkhdbieeh) |
| [EditThisCookie](https://chrome.google.com/webstore/detail/fngmhnnpilhplaeedifhccceomclgfbg) | [Config](./fngmhnnpilhplaeedifhccceomclgfbg) |
| [GIF Scrubber](https://chrome.google.com/webstore/detail/gbdacbnhlfdlllckelpdkgeklfjfgcmp) | [Config](./gbdacbnhlfdlllckelpdkgeklfjfgcmp) |
| [Quick Javascript Switcher](https://chrome.google.com/webstore/detail/geddoclleiomckbhadiaipdggiiccfje) | [Config](./geddoclleiomckbhadiaipdggiiccfje) |
| [Super Simple Highlighter](https://chrome.google.com/webstore/detail/hhlhjgianpocpoppaiihmlpgcoehlhio) | [Config](./hhlhjgianpocpoppaiihmlpgcoehlhio) |
| [Refined GitHub](https://chrome.google.com/webstore/detail/hlepfoohegkhhmjieoechaddaejaokhf) | [Config](./hlepfoohegkhhmjieoechaddaejaokhf) |
| [Auto Clicker - AutoFill](https://chrome.google.com/webstore/detail/iapifmceeokikomajpccajhjpacjmibe) | [Config](./iapifmceeokikomajpccajhjpacjmibe) |
| [FastForward](https://chrome.google.com/webstore/detail/icallnadddjmdinamnolclfjanhfoafe) | [Config](./icallnadddjmdinamnolclfjanhfoafe) |
| [Violentmonkey](https://chrome.google.com/webstore/detail/jinjaccalgkegednnccohejagnlnfdag) | [Config](./jinjaccalgkegednnccohejagnlnfdag) |
| [View Image Info (properties)](https://chrome.google.com/webstore/detail/jldjjifbpipdmligefcogandjojpdagn) | [Config](./jldjjifbpipdmligefcogandjojpdagn) |
| [LessPass](https://chrome.google.com/webstore/detail/lcmbpoclaodbgkbjafnkbbinogcbnjih) | [Config](./lcmbpoclaodbgkbjafnkbbinogcbnjih) |
| [Copy Element Text](https://chrome.google.com/webstore/detail/lejdjhmnhlhjhfmcalaimioiogkibdea) | [Config](./lejdjhmnhlhjhfmcalaimioiogkibdea) |
| ['Improve YouTube!' TEST](https://chrome.google.com/webstore/detail/lodjfjlkodalimdjgncejhkadjhacgki) | [Config](./lodjfjlkodalimdjgncejhkadjhacgki) |
| [SponsorBlock for YouTube - Skip Sponsorships](https://chrome.google.com/webstore/detail/mnjggcdmjocbbbhaepdhchncahnbgone) | [Config](./mnjggcdmjocbbbhaepdhchncahnbgone) |
| [Buster: Captcha Solver for Humans](https://chrome.google.com/webstore/detail/mpbjkejclgfgadiemmefgebjfooflfhl) | [Config](./mpbjkejclgfgadiemmefgebjfooflfhl) |
| [YouTube Anti Translate](https://chrome.google.com/webstore/detail/ndpmhjnlfkgfalaieeneneenijondgag) | [Config](./ndpmhjnlfkgfalaieeneneenijondgag) |
| [Keepa - Amazon Price Tracker](https://chrome.google.com/webstore/detail/neebplgakaahbhdphmkckjjcegoiijjo) | [Config](./neebplgakaahbhdphmkckjjcegoiijjo) |
| [Vue.js devtools](https://chrome.google.com/webstore/detail/nhdogjmejiglipccpnnnanhbledajbpd) | [Config](./nhdogjmejiglipccpnnnanhbledajbpd) |
| [Redirector](https://chrome.google.com/webstore/detail/ocgpenflpmgnfapjedencafcfakcekcd) | [Config](./ocgpenflpmgnfapjedencafcfakcekcd) |
| [LibRedirect](https://chrome.google.com/webstore/detail/oladmjdebphlnjjcnomfhhbfdldiimaf) | [Config](./oladmjdebphlnjjcnomfhhbfdldiimaf) |
| [Hover Zoom+](https://chrome.google.com/webstore/detail/pccckmaobkjjboncdfnnofkonhgpceea) | [Config](./pccckmaobkjjboncdfnnofkonhgpceea) |

Pinned :
- uBlock Origin
- Dark Reader
- Violentmonkey
- Quick Javascript Switcher
- Nimbus Screenshot & Screen Video Recorder
- Redirect Path
- LessPass