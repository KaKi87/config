const data =
    [...document
    .querySelector('extensions-manager')
    .shadowRoot
    .querySelector('#items-list')
    .shadowRoot
    .querySelectorAll('extensions-item')]
    .map(element => ({
        name: element
            .shadowRoot
            .querySelector('#name').textContent, id: element
            .shadowRoot
            .querySelector('#extension-id')
            .textContent
            .slice(4)
    }))
    .sort((a, b) => a.id.localeCompare(b.id))
    .map(item => `| [${item.name}](https://chrome.google.com/webstore/detail/${item.id}) | [Config](./${item.id}) |`);
console.log(`# Extensions

${data[0]}
| :--- | --- |
${data.slice(1).join('\n')}
`);