import axios from 'axios';

export default async (
    {
        method,
        path
    },
    {
        accessToken
    }
) => {
    if(method !== 'POST' || path !== '/user/settings') return;
    const client = axios.create({
        baseURL: 'https://git.kaki87.net/api/v1',
        params: {
            'access_token': accessToken 
        }
    });
    for(const item of (await client('/admin/users')).data){
        if(Date.now() - new Date(item['created']).valueOf() >= 3600000) continue;
        if(!item['website'] && !item['description']) continue;
        await client.delete(`/admin/users/${item['username']}`, { params: { 'purge': 'true' } });
    }
};